---

- name: Redhat | remi yum-utils
  ansible.builtin.yum: name=yum-utils state=present update_cache=yes
  register: pkg_result
  until: pkg_result is success

## for php-redis
- import_tasks: redhat-remi.yml
  when: ansible_os_family == "RedHat"

- name: RedHat8+ | Enable PowerTools repo
  ini_file:
    dest: /etc/yum.repos.d/CentOS-Stream-PowerTools.repo
    section: powertools
    option: "{{ item.o }}"
    value: "{{ item.v }}"
    mode: '0644'
  with_items:
    - { o: enabled, v: '1' }
    - { o: baseurl, v: 'http://mirror.centos.org/$contentdir/$releasever/PowerTools/$basearch/os/' }
  when:
    - ansible_os_family == "RedHat"
    - ansible_distribution_major_version | int >= 8

- name: Check available repositories
  ansible.builtin.command: dnf repolist
  changed_when: false
  when:
    - ansible_os_family == "RedHat"
    - ansible_distribution_major_version | int >= 8

- name: Redhat | MISP dependencies install
  ansible.builtin.yum:
    name: "{{ misp_pkg_list + webserver }}"
    state: present
    update_cache: yes
  async: 3600
  poll: 300
  register: pkg_result
  until: pkg_result is success

- name: Nginx
  when: misp_webserver is defined and misp_webserver == 'nginx'
  block:
    - name: Set php-fpm to use socket file
      ansible.builtin.replace:
        dest: /etc/php-fpm.d/www.conf
        regexp: "{{ item.re }}"
        replace: "{{ item.rep }}"
        mode: '0644'
        backup: yes
      with_items:
        - { re: '^listen = .*$', rep: 'listen = /var/run/php-fpm/php-fpm.sock' }
        - { re: '^;listen.owner = nobody', rep: 'listen.owner = {{ fpm_user }}' }
        - { re: '^;listen.group = nobody', rep: 'listen.group = {{ fpm_user }}' }
        - { re: '^listen.owner = .*$', rep: 'listen.owner = {{ fpm_user }}' }
        - { re: '^listen.group = .*$', rep: 'listen.group = {{ fpm_user }}' }
      notify:
        - Restart php-fpm

    - name: Ensure socket dir exists
      ansible.builtin.file: dest=/var/run/php-fpm state=directory owner=nobody group=nobody mode=0755

    - name: Set php-fpm user/group to nginx
      ansible.builtin.replace:
        dest: /etc/php-fpm.d/www.conf
        regexp: "{{ item.re }}"
        replace: "{{ item.rep }}"
        mode: '0644'
        backup: yes
      with_items:
        - { re: '^user = .*$', rep: 'user = nginx' }
        - { re: '^group = .*$', rep: 'group = nginx' }
      notify:
        - Restart php-fpm

- name: Redhat | ensure python36 site-packages directory exists
  ansible.builtin.file:
    dest: /usr/local/lib/python3.6/site-packages
    owner: root
    mode: '0755'
    state: directory

# https://github.com/ansible/ansible/issues/16612
- name: RedHat selinux facts
  when: ansible_os_family == "RedHat"
  block:
    - name: Debug | ansible_selinux var
      ansible.builtin.debug: var=ansible_selinux
    - name: RedHat7- | Ensure selinux dependencies are present
      ansible.builtin.package:
        name:
          - libselinux-python
          - libsemanage-python
          - policycoreutils-python
        state: present
      register: pkg_result
      until: pkg_result is success
      when:
        - ansible_os_family == "RedHat"
        - ansible_distribution_major_version|int < 8
    - name: RedHat8+ | Ensure selinux dependencies are present
      ansible.builtin.package:
        name:
          - python3-libselinux
          - python3-libsemanage
          - python3-policycoreutils
        state: present
      register: pkg_result
      until: pkg_result is success
      when:
        - ansible_os_family == "RedHat"
        - ansible_distribution_major_version | int >= 8
    - name: Re-collect facts
      ansible.builtin.setup:
    - name: Debug | ansible_selinux var
      ansible.builtin.debug: var=ansible_selinux
- name: RedHat selinux
  when:
    - ansible_os_family == "RedHat"
    - ansible_selinux.status is defined
    - ansible_selinux.status != 'disabled'
  block:
    - name: RedHat | Allow proxy to network connect in selinux
      seboolean:
        name: httpd_can_network_connect
        state: yes
        persistent: yes

    - name: RedHat | Allow httpd to listen to unusual ports
      seport:
        ports: "{{ misp_base_port }}"
        proto: tcp
        setype: http_port_t
        state: present
      when: misp_base_port != 80 and misp_base_port != 443
